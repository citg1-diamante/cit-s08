--a. Find all artists that has letter D in its name. 
SELECT * FROM artists WHERE name LIKE "%d%";

--b. Find all songs that has length of less than 230. 
SELECT * FROM songs WHERE length < 230;

--c. Join the albums and songs tables
SELECT album_title, song_name, length FROM albums
    JOIN songs ON albums.id = songs.album_id;

--d. Join the artists and albums tables
SELECT * FROM artists
    JOIN albums ON artists.id = albums.artist_id WHERE album_title LIKE "%a%";

--e. Sort tha album in Z-A order
SELECT * FROM albums ORDER BY album_title DESC LIMIT 4;

--f. Join the albums and songs tables
SELECT * FROM albums
    JOIN songs ON albums.id = songs.album_id ORDER BY album_title DESC, song_name;